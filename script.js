const valores = [];

function adicionarValor() {
    const valorInput = document.getElementById('valor').value;

    if (valorInput.trim() === "") {
        alert('Por favor, insira uma nota.');
        return;
    }

    const valor = parseFloat(valorInput.replace(',', '.')).toFixed(2);

    if (isNaN(valor) || valor < 0 || valor > 10) {
        alert('A nota digitada é inválida, por favor, insira uma nota válida entre 0 e 10.');
        return;
    }

    valores.push(parseFloat(valor));
    document.getElementById('valor').value = '';

    const listaValoresAdicionados = document.getElementById('valores-adicionados');
    const item = document.createElement('li');
    item.textContent = `A nota ${valores.length} foi: ${valor}`;
    listaValoresAdicionados.appendChild(item);
}

function calcularMedia() {
    if (valores.length === 0) {
        alert('Insira pelo menos um valor antes de calcular a média.');
        return;
    }

    const totalValores = valores.length;
    const soma = valores.reduce((acc, num) => acc + num, 0);
    const media = soma / totalValores;

    const resultadoElement = document.getElementById('resultado');
    resultadoElement.innerText = `A média é: ${media.toFixed(2).replace('.', ',')}`;
}
